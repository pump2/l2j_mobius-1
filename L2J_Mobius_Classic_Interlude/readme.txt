L2J-Mobius Classic Interlude

Client: https://drive.google.com/uc?id=1_kioRKR5FTOmINAtUQe0KCxrvPzY18Nd&export=download
Patch: https://www.mediafire.com/file/dn0w50i8659jrxs/L2J_Mobius_Classic_Interlude_Patch_v10.zip
System: https://www.mediafire.com/file/mvergqghqz4jzpq/L2J_Mobius_Classic_Interlude_System_v11.zip

JDK: http://www.mediafire.com/file/cgh3zupv80qdwv4/bellsoft-jdk15.0.2%252B10-windows-amd64.msi
Eclipse: http://www.mediafire.com/file/h0gmazpv9hm6gjp/eclipse-java-2020-12-R-win32-x86_64.zip
Geodata: http://www.mediafire.com/file/prx3sls7lts5p1f/L2J_Mobius_Classic_Interlude_Geodata.zip


This is a Classic server based on the Grand Crusade client.
The goal is to make a better approximation of what Classic is to older chronicles, like Interlude.
Never the less this is still Classic, do not expect a pure Interlude version.
It is shared with the hope that more people will be involved and help with the development.
Who knows? Maybe some day it will be a pure Interlude version.

A lot of things can go wrong while using this project,
if you do not know what you are doing, it is best not to use it.

Tools that might be helpful (use with Java 1.8)
L2ClientDat: https://github.com/MobiusDevelopment/l2clientdat
XdatEditor: https://github.com/MobiusDevelopment/xdat_editor
L2Tool: https://github.com/MobiusDevelopment/l2tool